/* Copyright 2020 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of ninos.
 *
 * ninos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ninos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ninos.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NIN_EVDEV_HH
#define NIN_EVDEV_HH

#include "async_poll.hh"

#include <algorithm>
#include <stdexcept>
#include <string>

#include <libevdev/libevdev.h>

namespace nin {

/* Event handler is a virtual function that must
 * be specialized by the user to handle the events:
protected:
void on_event(input_event const& event) override;
 *
 *
 * These are some quick struct information:
 *
 * struct input_event
 * {
 *     timeval  time;
 *     unsigned type;
 *     unsigned code;
 *     int      value;
 * };
 * struct timeval
 * {
 *     time_t      tv_sec;
 *     suseconds_t tv_usec;
 * };
 */
class evdev
{
public:
    evdev();
    evdev(evdev const& copy) = delete;
    //evdev(evdev     && move);
    evdev(char const* device_path);

    virtual ~evdev();

    void open(char const* device_path);
    void close();

public: /* Capabilities */

    /* List device metadata */
    struct evdev_metadata_t {
        std::string name;
        std::string physical_location;
        std::string unique_identifier;
        struct {
            int product;
            int vendor;
            int bustype;
            int version;
        } ID;
        int driver_version;
    };

    evdev_metadata_t const& metadata() const
    { return metadata_cache; }


    /* List all events and codes */
    struct evdev_codes_t
    {
        std::string name;
        std::map<unsigned int, std::string> codes;
    };
    using evdev_type_to_codes_t = std::map<unsigned int, evdev_codes_t>;

    evdev_type_to_codes_t const& event_types() const
    { return event_types_cache; }


    /* List properties of event types absolute EV_ABS */
    struct evdev_absinfo_t
    {
        int minimum;
        int maximum;
        int fuzz;
        int flat;
        int resolution;
    };
    using evdev_abscode_to_absinfo_t = std::map<unsigned int, evdev_absinfo_t>;

    evdev_absinfo_t const& absinfo(unsigned int code) const
    { return abs_infos_cache.at(code); }


protected:

    /* Users specialize this function in a derived class */
    virtual void on_event(input_event const& event) { }

    double normalize_value(input_event const& event);

private:
    int file_descriptor;
    ::libevdev * device = nullptr;
    operator ::libevdev * ()
    { return device; }

    evdev_metadata_t metadata_cache;
    void fill_metadata_cache();

    evdev_type_to_codes_t event_types_cache;
    void fill_event_types_cache();

    evdev_abscode_to_absinfo_t abs_infos_cache;
    void fill_absinfos();

    static void event_callback(evdev * device);
    static async_poll<evdev *> poller;
};

//evdev
//evdev_factory(unsigned int evdev_file_suffix);

std::vector<std::unique_ptr<evdev>>
evdev_scan_all();

} // namespace nin

#endif
