/* Copyright 2020 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of ninos.
 *
 * ninos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ninos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ninos.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NIN_ASYNC_POLL_HH
#define NIN_ASYNC_POLL_HH

#include <functional>
#include <mutex>
#include <map>
#include <thread>

#include <poll.h>
#include <unistd.h>
#include <fcntl.h>

namespace nin {

/* async_poll - Low level poll of file descriptors with a single thread.
 *
 * One thread per object. When data is available in one of the file descriptors
 * on watch, it will call the callback given at construction with the data
 * supplied at registration of the file descriptor.
 * data_t is most often a pointer type.
 *
 * Before closing a file descriptor, make sure that watching() is false:
 * After a call to sync, all watch/drop requests will be completed (thus
 * watching() will be false).
 * The destructor drops all file descriptors before returning.
 *
 * fd_update_latency_us: Set to a lower value in applications with lots
 * of watch/drop requests.
 */

template <typename data_t>
class async_poll
{
    std::function<void (data_t)> user_callback;

    using fd_t = int;
    std::jthread loop_thread;
    std::mutex   loop_mtx;

    void loop();

    static bool fd_readable(fd_t fd);
    static void wait(unsigned int duration_us);

    std::map<fd_t, data_t> watch;
    std::map<fd_t, data_t> on_watch;
    std::vector<fd_t> drop;

public:

    async_poll(std::function<void (data_t)> const& callback);
    async_poll(async_poll const& copy) = delete;
    ~async_poll();

    // 'request_watch' replaces data if file_descriptor is on watch.
    void request_watch(int file_descriptor, data_t data);
    bool watching(int file_descriptor) const;
    void request_drop(int file_descriptor);
    // TODO void drop_all();
    void sync(); // Waits for all watch/drop requests to be completed.

    unsigned int fd_update_latency_us = 500'000;
};

/*________________________________ DEFINITIONS _______________________________*/


template <typename data_t>
async_poll<data_t>:: async_poll(std::function<void (data_t)> const& callback)
    : user_callback {callback}
    , loop_thread   {&async_poll::loop, this}
{
}

template <typename data_t>
async_poll<data_t>:: ~async_poll()
{
    loop_thread.request_stop();
}

template <typename data_t>
void
async_poll<data_t>:: loop()
{
    std::stop_token stop = loop_thread.get_stop_token();
    std::vector<pollfd> fdss;
    bool reconstruct_fdss = true;

    while (!stop.stop_requested())
    {
        // Synhronize buffers
        if (!watch.empty())
        {
            std::scoped_lock lock {loop_mtx};
            for (auto const& p : watch)
                on_watch[p.first] = p.second;
            watch.clear();
            reconstruct_fdss = true;
        }

        if (!drop.empty())
        {
            std::scoped_lock lock {loop_mtx};
            for (auto idx : drop)
                on_watch.erase(idx);
            drop.clear();
            reconstruct_fdss = true;
        }

        // Update poll() arguments
        if (reconstruct_fdss)
        {
            fdss.clear();
            for (auto const& p : on_watch)
                fdss.emplace_back( p.first, POLLIN, 0 );
            reconstruct_fdss = false;
        }

        // Poll
        if (on_watch.empty())
        {
            wait(fd_update_latency_us);
            continue;
        }
        int poll_event_n = poll(fdss.data(), fdss.size(),
                fd_update_latency_us/1024 );
        if (poll_event_n == 0)
            continue;

        // Call callbacks
        for (auto & ev : fdss)
        if (ev.revents)
        {
            ev.revents = 0;
            user_callback(on_watch[ev.fd]);
        }
    }
}

template <typename data_t>
bool
async_poll<data_t>:: fd_readable(fd_t fd)
{
    return !( fcntl(fd, F_GETFL) & (O_WRONLY) );
}

template <typename data_t>
void
async_poll<data_t>:: request_drop(int file_descriptor)
{
    std::scoped_lock lock {loop_mtx};
    watch.erase(file_descriptor);
    if (on_watch.contains(file_descriptor))
        drop.push_back(file_descriptor);
}

template <typename data_t>
void
async_poll<data_t>:: request_watch(int file_descriptor, data_t data)
{
    if (fd_readable(file_descriptor))
    {
        std::scoped_lock lock {loop_mtx};
        watch[file_descriptor] = data;
    }
    else
        throw std::runtime_error("fd is not open in read mode");
}

template <typename data_t>
void
async_poll<data_t>:: sync()
{
        unsigned int wait_us = fd_update_latency_us / 4;
        while (!watch.empty() || !drop.empty())
            wait(wait_us);
}

template <typename data_t>
void
async_poll<data_t>:: wait(unsigned int duration_us)
{
    unsigned int duration_s = duration_us / 1'000'000;
    unsigned int duration_us_only = duration_us % 1'000'000;
    sleep(duration_s);
    usleep(duration_us_only);
}

template <typename data_t>
bool
async_poll<data_t>:: watching(int file_descriptor) const
{
    std::scoped_lock lock {loop_mtx};
    return on_watch.contains(file_descriptor);
}


} // namespace nin

#endif
