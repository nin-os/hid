# HID LIBRARY for Ninbot


NinOS HID is a CORBA bridge for HID devices written in C++ for the NinOS middleware.

## FEATURES

- ...


## REQUIREMENTS

* A C++20 compiler

## USAGE

### Testing ioserial

Use cmake to compile the sources and then `ctest`, or run the tests manually.


## CONTRIBUTING AND BUG REPORTS

Please open an issue in GitLab, a merge request or contact the authors directly.

## LICENSE

NinOS HID is distributed under the terms of the General Public License, version 3 or later.

## AUTHORS

Copyright 2020 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>


## TODO

- In Linux with nintendo controllers, require dkms-hid-nintendo and joycond in CMakeLists for full support. This driver is in review on the linux-input mailing list.
- Add support for Windows OS

